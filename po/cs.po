# Czech translation for gPlanarity
# Copyright (C) 2007
# This file is distributed under the same license as the gPlanarity package.
# Petr Pisar <petr.pisar@atlas.cz>, 2007
#
msgid ""
msgstr ""
"Project-Id-Version: gPlanarity 11496\n"
"Report-Msgid-Bugs-To: Monty <monty@xiph.org>\n"
"POT-Creation-Date: 2009-11-25 02:29-0500\n"
"PO-Revision-Date: 2007-05-15 17:58+0200\n"
"Last-Translator: Petr Pisar <petr.pisar@atlas.cz>\n"
"Language-Team: Czech <translation-team-cs@lists.sourceforge.net>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

msgid "exit gPlanarity"
msgstr "ukončit gPlanarity"

msgid "level selection menu"
msgstr "nabídka výběru kol"

#, fuzzy
msgid "play next level!"
msgstr "hrát kolo!"

msgid "Level Complete!"
msgstr "Kolo hotovo!"

#, c-format
msgid "Elapsed: %s"
msgstr "Uplynulo: %s"

#, c-format
msgid "Base score: %d points"
msgstr "Základní skóre: %d bodů"

#, c-format
msgid "Objective Exceeded! %d%%"
msgstr "Úkol překonán! %d%%"

#, c-format
msgid "Time bonus: %d points"
msgstr "Časový bonus: %d bodů"

#, c-format
msgid "Final score: %d points"
msgstr "Konečné skóre: %d bodů"

msgid "A high score!"
msgstr "Rekord!"

#, c-format
msgid "Previous best: %ld points"
msgstr "Předchozí nejlepší: %ld bodů"

msgid "Total score to date:"
msgstr "Celkové skóre k datu:"

#, c-format
msgid "%ld points"
msgstr "%ld bodů"

msgid "reset level"
msgstr "znovu začít kolo"

msgid "play level!"
msgstr "hrát kolo!"

msgid "Available Levels"
msgstr "Dostupná kola"

#, c-format
msgid "Level %d:"
msgstr "Kolo %d:"

#, c-format
msgid "[not yet completed]"
msgstr "[ještě nedokončeno]"

#, c-format
msgid "level high score: %ld"
msgstr "rekord pro kolo: %ld"

#, c-format
msgid "total score all levels: %ld"
msgstr "celkové skóre za všechna kola: %ld"

msgid "resume game!"
msgstr "pokračovat ve hře!"

msgid "Game Paused"
msgstr "Hra zastavena"

msgid "Time Elapsed:"
msgstr "Uplynulý čas:"

msgid "gPlanarity"
msgstr "gPlanarity"

msgid "Untangle the mess!"
msgstr "Rozmotejte šmodrchanec!"

msgid "Drag vertices to eliminate crossed lines."
msgstr "Přesunujte vrcholy, abyste odstranili křížící se čáry."

msgid "The objective may be a complete solution or"
msgstr "Zadání může být uplné řešení nebo"

msgid "getting as close as possible to solving an"
msgstr "dobrání se podoby co nejblížší řešení"

msgid "unsolvable puzzle.  Work quickly and"
msgstr "neřešitelného rébusu. Pracujte rychle a"

msgid "exceed the objective for bonus points!"
msgstr "získejte body navíc za překonání zadání!"

msgid "gPlanarity written by Monty <monty@xiph.org>"
msgstr "gPlanarity napsal Monty <monty@xiph.org>"

msgid "as a demonstration of Gtk+/Cairo"
msgstr "jako demo Gtk+/Caira"

msgid "Original Flash version of Planarity by"
msgstr "Původní verzi Planarity ve flashy napsal"

msgid "John Tantalo <john.tantalo@case.edu>"
msgstr "John Tantalo <john.tantalo@case.edu>"

msgid "Original game concept by Mary Radcliffe"
msgstr "Koncepci hry vymysla Mary Radcliffe."

#, c-format
msgid ""
"\n"
"\n"
"ERROR: The windowmanager appears to be ignoring resize requests.\n"
"This stands a pretty good chance of scrambling any saved board larger\n"
"than the default window size.\n"
"\n"
msgstr ""
"\n"
"\n"
"CHYBA: Zdá se, že správce oken ignoruje požadavky na změnu velikosti okna.\n"
"Což znamená docela dobrou šanci, že uložená deska větší než výchozí "
"velikost\n"
"okna bude poškozená.\n"
"\n"

#, c-format
msgid ""
"Clipping and/or expanding this board to the current window size...\n"
"\n"
msgstr ""
"Lze očekávat oříznutí a/nebo roztažení takové desky na rozměry okna…\n"
"\n"

#, c-format
msgid ""
"\n"
"\n"
"ERROR: The window size granted by the windowmanager is not the\n"
"window size gPlanarity requested.  If the windowmanager is\n"
"configured to ignore application sizing requests, this stands\n"
"a pretty good chance of scrambling saved boards later (and\n"
"making everything look funny now).\n"
"\n"
msgstr ""
"\n"
"\n"
"CHYBA: Požadovaná velikost okna gPlanarity a velikost zaručena správcem "
"oken\n"
"se liší. Pokud je správce oken nastaven, aby ignoroval požadavky na změnu\n"
"velikosti okna, je docela dobrá šance na poškození později uložených desek\n"
"(a na veselý vzhled čehokoliv již od teď).\n"
"\n"

msgid "rather many, really"
msgstr "raději mnoho, opravdu"

#, c-format
msgid "ERROR:  Could not load board icon \"%s\"\n"
msgstr "CHYBA: Nelze načíst ikonu desky „%s“\n"

#, c-format
msgid "ERROR:  Could not save board icon \"%s\"\n"
msgstr "CHYBA: nelze uložit ikonu desky „%s“\n"

#, c-format
msgid "Level %d: %s"
msgstr "Kolo %d: %s"

#, c-format
msgid "Score: %d"
msgstr "Skóre: %d"

#, c-format
msgid "%d%%"
msgstr "%d%%"

#, c-format
msgid "Intersections: %ld"
msgstr "Průniků: %ld"

#, c-format
msgid "Objective: %s"
msgstr "Zadání: %s"

msgid "reset board"
msgstr "resetovat desku"

msgid "pause"
msgstr "pauza"

msgid "help / about"
msgstr "nápověda / o programu"

msgid "expand"
msgstr "roztáhnout"

msgid "shrink"
msgstr "smrštit"

msgid "hide/show lines"
msgstr "skrýt/zobrazit čáry"

msgid "mark intersections"
msgstr "označit průniky"

msgid "click when finished!"
msgstr "po dokončení klikněte!"

#, c-format
msgid ""
"ERROR:  Could not save board state for \"%s\":\n"
"\t%s\n"
msgstr ""
"CHYBA: Nelze uložit stav desky pro „%s“:\n"
"\t%s\n"

#, c-format
msgid ""
"ERROR:  Could not read saved board state for \"%s\":\n"
"\t%s\n"
msgstr ""
"CHYBA: Nelze načíst uložený stav desky pro „%s“:\n"
"\t%s\n"

#, c-format
msgid "WARNING: edge references out of range vertex in save file\n"
msgstr "VAROVÁNÍ: hrana odkazuje na vrchol mimo rozsah uložený v souboru\n"

#, c-format
msgid "Couldn't allocate memory for level name.\n"
msgstr "Nelze vyhradit paměť pro jméno kola.\n"

#, c-format
msgid "Couldn't allocate memory for level description.\n"
msgstr "Nelze vyhradit paměť pro popis kola.\n"

#, c-format
msgid ""
"region overlap adjustment failed in arc_arc_adj; \n"
"  This is an internal error that should never happen.\n"
msgstr ""
"přizpůsobení překrývajících se oblastí selhalo v arc_arc_arj;\n"
"  Toto je vnitřní chyba a nemělo by k ní nikdy dojít.\n"

msgid "zero intersections"
msgstr "žádné průniky"

msgid "1 intersection or fewer"
msgstr "1 průnik nebo méně"

msgid "1 intersection"
msgstr "1 průnik"

#, c-format
msgid "%d intersections%s"
msgstr "%d průniků%s"

msgid " or fewer"
msgstr " nebo méně"

#, c-format
msgid ""
"ERROR:  Could not save game state file \"%s\":\n"
"\t%s\n"
msgstr ""
"CHYBA:  Nelze uložit soubor se stavem hry „%s“:\n"
"\t%s\n"

#, c-format
msgid ""
"ERROR:  Could not read game state file \"%s\":\n"
"\t%s\n"
msgstr ""
"CHYBA:  Nelze načíst soubor se stavem hry „%s“:\n"
"\t%s\n"

#, c-format
msgid ""
"ERROR:  Could not create directory (%s) to save game state:\n"
"\t%s\n"
msgstr ""
"CHYBA:  Nelze vytvořit adresář (%s) pro ukládání stavu hry:\n"
"\t%s\n"

#, c-format
msgid ""
"\n"
"Trapped signal %d; saving state and exiting!\n"
msgstr ""
"\n"
"Zachycen signál %d; uložím stav a skončím!\n"

#, c-format
msgid ""
"\n"
"Unable to find any suitable %s fonts!\n"
"Continuing, but the the results are likely to be poor.\n"
"\n"
msgstr ""
"\n"
"Nelze najít žádný vhodný font s %s!\n"
"Pokračuji, ale výsledek asi bude ošklivý.\n"
"\n"

msgid "bold italic"
msgstr "tučnou kurzívou"

msgid "italic"
msgstr "kurzívou"

msgid "bold"
msgstr "tučným řezem"

msgid "medium"
msgstr "středním řezem"

#, c-format
msgid ""
"\n"
"Selected %s font \"%s\" is not scalable!  This is almost as bad\n"
"as not finding any font at all.  Continuing, but this may look\n"
"very poor indeed.\n"
"\n"
msgstr ""
"\n"
"Vybraný font s %s „%s“ není škálovatelný!\n"
"Toto situace je téměř tak špatná jako když se nenajde font žádný.\n"
"Pokračuji, ale výsledek bude asi ošklivý\n"
"\n"

#, c-format
msgid ""
"No homedir environment variable set!  gPlanarity will be\n"
"unable to permanently save any progress or board state.\n"
msgstr ""
"Žádná proměnná prostředí neudává domovský adresář! gPlanatiry nebude "
"schopné\n"
"trvale uložit postup hráče nebo stav hrací desky.\n"

#, c-format
msgid "%d:%02d:%02d"
msgstr "%d:%02d:%02d"

#, c-format
msgid "%d:%02d"
msgstr "%d:%02d"

#, c-format
msgid "%d seconds"
msgstr "%d sekund"
